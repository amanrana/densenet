# DenseNet

PyTorch implementation of DenseNet: Densely Connected Convolutional Networks [https://arxiv.org/abs/1608.06993]