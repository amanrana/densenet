#!/usr/bin/python3

"""
PyTorch model architecture code for Densely Connected CNN
@author: Aman Rana

date: 3 March 2019
"""

import math
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torchvision.utils import save_image
from torch.utils.data import DataLoader

from tqdm import tqdm


class Bottleneck(nn.Module):
    """Dense block."""
    def __init__(self, in_channels, out_chanels):
        super(Bottleneck, self).__init__()
        self.bn1 = nn.BatchNorm2d(in_channels)
        self.conv2d_1 = nn.Conv2d(in_channels=in_channels,
                                  out_channels=4 * out_chanels,
                                  kernel_size=1)
        self.bn2 = nn.BatchNorm2d(4 * out_chanels)
        self.conv2d_2 = nn.Conv2d(in_channels=4 * out_chanels,
                                  out_channels=out_chanels,
                                  kernel_size=3, padding=1)

    def forward(self, x):
        """Forward pass."""
        output = self.conv2d_1(F.relu(self.bn1(x)))
        output = self.conv2d_2(F.relu(self.bn2(output)))
        output = torch.cat((x, output), 1)
        return output


class TransitionLayer(nn.Module):
    """Transition layer between the dense blocks."""
    def __init__(self, in_channels, out_channels):
        super(TransitionLayer, self).__init__()
        self.bn1 = nn.BatchNorm2d(in_channels)
        self.conv2d_1 = nn.Conv2d(in_channels=in_channels,
                                  out_channels=out_channels,
                                  kernel_size=1)

    def forward(self, x):
        out = self.conv2d_1(F.relu(self.bn1(x)))
        out = F.avg_pool2d(out, 2)
        return out


class DenseNet(nn.Module):
    """Densely Connected Convolution Network architecture."""
    def __init__(self, depth, growth_rate, n_classes):
        super(DenseNet, self).__init__()
        g_rate = growth_rate
        n_bottle_layers = (depth // 3) // 2

        out_ch = 2 * g_rate
        self.conv2d_1 = nn.Conv2d(3, out_ch, kernel_size=3,
                                  padding=1)

        # Dense block + Transition layer (1)
        in_ch = out_ch
        self.dense_block_1 = self.dense_block(in_ch,
                                              n_bottle_layers,
                                              g_rate)
        in_ch += (n_bottle_layers * g_rate)
        out_ch = int(math.floor(in_ch * 0.5))
        self.transition_1 = TransitionLayer(in_ch, out_ch)

        # Dense block + Transition layer (2)
        in_ch = out_ch
        self.dense_block_2 = self.dense_block(out_ch,
                                              n_bottle_layers,
                                              g_rate)
        in_ch += (n_bottle_layers * g_rate)
        out_ch = int(math.floor(in_ch * 0.5))
        self.transition_2 = TransitionLayer(in_ch, out_ch)

        # Dense block + Transition layer (3)
        in_ch = out_ch
        self.dense_block_3 = self.dense_block(out_ch,
                                              n_bottle_layers,
                                              g_rate)
        in_ch += (n_bottle_layers * g_rate)

        # BatchNorm + Output layer
        self.bn1 = nn.BatchNorm2d(in_ch)
        self.fc = nn.Linear(in_ch, n_classes)
        self.log_softmax = nn.LogSoftmax(dim=0)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.bias.data.zero_()

    def dense_block(self, in_channels, n_bottle_layers, growth_rate):
        """Defining dense block."""
        layers = []
        for i in range(n_bottle_layers):
            layers.append(Bottleneck(in_channels, growth_rate))
            in_channels += growth_rate
        return nn.Sequential(*layers)

    def forward(self, x):
        output = self.conv2d_1(x)
        output = self.transition_1(self.dense_block_1(output))
        output = self.transition_2(self.dense_block_2(output))
        output = self.dense_block_3(output)
        output = torch.squeeze(F.avg_pool2d(F.relu(self.bn1(output)),
                                            8))
        output = self.log_softmax(self.fc(output))
        return output


class Model(object):
    def __init__(self, args, cuda_available):
        self.args = args
        self.cuda_available = cuda_available

    @staticmethod
    def dense_connected_cnn(depth, growth_rate, n_classes):
        """Returns DenseNet model."""
        return DenseNet(depth, growth_rate, n_classes)

    def train(self, model, data, optimizer, epoch, loss_runner,
              wrong_runner, error_runner, batch_size, pbar1,
              cuda_device, save=True):
        """Train the CNN."""
        # Set mode to train
        model.train()

        for iteration, (x, y) in enumerate(data):
            # Transfer data to GPU (if available)
            if self.cuda_available is True:
                x = x.to(cuda_device)
                y = y.to(cuda_device)

            x = torch.cuda.FloatTensor(x)
            y = torch.cuda.LongTensor(y)

            optimizer.zero_grad()  # Setting gradients to zero
            y_hat = model(x)  # Model output
            loss = F.nll_loss(input=y_hat, target=y)
            loss.backward()
            optimizer.step()

            y_hat_max = y_hat.data.max(1)[1]
            incorrect = y_hat_max.ne(y.data).cpu().sum()
            error = int(incorrect) / float(batch_size)
            error_percent = 100.0 * error

            # Calculate accuracy and loss
            loss_val = loss.data.item()

            # Update loss and error runners
            loss_runner.update(loss_val, batch_size)
            wrong_runner.update(incorrect, batch_size)
            error_runner.update(error_percent, batch_size)

            msg = "Epoch {a}/{b} || Loss {c:.2f} || Errors: {d:.2f} ({e:.2f}%)"
            msg = msg.format(a=epoch,
                             b=iteration+1,
                             c=loss_runner.average,
                             d=wrong_runner.average,
                             e=error_runner.average)
            pbar1.set_description(msg)

        return model

    def test(self, model, data, loss_runner, wrong_runner,
             error_runner, batch_size, cuda_device):
        """Test performance on the testing dataset.
        Returns test loss & accuracy"""
        # Set mode to eval
        torch.cuda.empty_cache()
        torch.set_grad_enabled(False)
        model.eval()

        pbar1 = tqdm(enumerate(data), position=0)
        for iteration, (x, y) in pbar1:
            # Transfer data to GPU (if available)
            if self.cuda_available is True:
                x = x.to(cuda_device)
                y = y.to(cuda_device)

            # with torch.no_grad():
            x = torch.cuda.FloatTensor(x)
            y = torch.cuda.LongTensor(y)

            y_hat = model(x)  # Model output
            loss = F.nll_loss(input=y_hat, target=y)

            y_hat_max = y_hat.data.max(1)[1]
            incorrect = y_hat_max.ne(y.data).cpu().sum()
            error = int(incorrect) / float(batch_size)
            error_percent = 100.0 * error

            # Calculate accuracy and loss
            loss_val = loss.data.item()

            # Update loss and error runners
            loss_runner.update(loss_val, batch_size)
            wrong_runner.update(incorrect, batch_size)
            error_runner.update(error_percent, batch_size)

            msg = "Iteration {a} || Loss {b:.2f} || Errors: {c:.2f} ({d:.2f}%)"
            msg = msg.format(a=iteration+1,
                             b=loss_runner.average,
                             c=wrong_runner.average,
                             d=error_runner.average)
            pbar1.set_description(msg)

        print("\nAverage loss: {}".format(loss_runner.average))
        print("Average wrong: {}".format(wrong_runner.average))
        print("Average error: {}%".format(error_runner.average))


if __name__ == "__main__":
    model = Model()
