#!/usr/bin/python3

"""
PyTorch model architecture code for Densely Connected CNN
@author: Aman Rana

date: 3 March 2019
"""

import torch
import torch.optim as optim
from torch.autograd import Variable
import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torch.utils.data import DataLoader

# For model summary
from torchsummary import summary

from torchvision.utils import save_image
from model import Model  # Load model
from tqdm import tqdm
import argparse

from utils import RunningAverage


class DenselyConnectedCNN(object):
    """Densely connected cnn train and test code."""
    def __init__(self, args):
        self.args = args
        self.cuda_available = torch.cuda.is_available()

    def load_dataset(self, d: str):
        """Load the specified dataset.
        Arguments:
            d: dataset to load.

        Returns
            (image, target) where target is index of the target class.
        """
        normMean = [0.49139968, 0.48215827, 0.44653124]
        normStd = [0.24703233, 0.24348505, 0.26158768]
        # normTransform = transforms.Normalize(normMean, normStd)

        train_transforms = transforms.Compose([
            transforms.RandomHorizontalFlip(),
            # transforms.ColorJitter(0.1, 0.1, 0.1, 0.1)
            transforms.ToTensor(),
            transforms.Normalize(normMean, normStd)
        ])

        test_transforms = testTransform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(normMean, normStd)
        ])

        _transforms = {
            "train": train_transforms,
            "test": testTransform
        }

        dataset_map = {
            "cifar10": datasets.CIFAR10,
            "cifar100": datasets.CIFAR100,
            "svhn": datasets.SVHN
        }

        data_fn = dataset_map[d]
        train = DataLoader(data_fn(root="/tmp/cifar", train=True,
                                   transform=_transforms["train"],
                                   download=True),
                           batch_size=self.args.batch_size,
                           shuffle=True, num_workers=1)
        test = DataLoader(data_fn(root="/tmp/cifar", train=False,
                                  transform=_transforms["test"],
                                  download=True),
                          batch_size=self.args.batch_size,
                          shuffle=False, num_workers=1)

        return train, test

    def __get_optimizer(self, model, lr):
        """Returns the pytorch optimizer."""
        if self.args.optimizer == 'sgd':
            optimizer = optim.SGD(model.parameters(),
                                  lr=lr,
                                  momentum=0.9,
                                  weight_decay=1e-4)
        elif self.args.optimizer == 'adam':
            optimizer = optim.Adam(model.parameters(),
                                   weight_decay=1e-4)
        elif self.args.optimizer == 'rmsprop':
            optimizer = optim.RMSprop(model.parameters(),
                                      weight_decay=1e-4)
        else:
            raise ValueError("Set optimizer correctly !")

        return optimizer

    def main(self):
        """Main. Start training + testing based on user specified
        dataset."""
        cuda0 = torch.device('cuda:0')

        # Load datasets
        train_data, test_data = self.load_dataset(d=args.dataset)

        # Initialize model
        model = Model(self.args, self.cuda_available)
        densenet = model.dense_connected_cnn(depth=100,
                                             growth_rate=12,
                                             n_classes=10)
        # print(summary(densenet, input_size=(3, 32, 32)))

        # TODO: Print number of parameters in model
        msg = "Number of parameters in DenseNet model is: {}"
        mp = densenet.parameters()
        print(msg.format(sum([p.data.nelement() for p in mp])))

        if self.cuda_available:
            densenet.cuda(cuda0)
            print("Using GPU (available)")
        else:
            print("Using CPU")

        # Start training for N epochs
        print("\nStarting training..")
        loss_runner = RunningAverage()
        wrong_runner = RunningAverage()
        error_runner = RunningAverage()

        pbar1 = tqdm(range(1, args.epochs + 1), position=0)
        for epoch in pbar1:
            if epoch <= 5:
                optimizer = self.__get_optimizer(densenet, lr=1e-1)
            elif 5 < epoch <= 10:
                optimizer = self.__get_optimizer(densenet, lr=1e-2)
            elif 10 < epoch <= 15:
                optimizer = self.__get_optimizer(densenet, lr=1e-3)
            elif 15 < epoch <= 20:
                optimizer = self.__get_optimizer(densenet, lr=1e-4)
            else:
                optimizer = self.__get_optimizer(densenet, lr=1e-5)

            densenet = model.train(model=densenet,
                                   data=train_data,
                                   optimizer=optimizer,
                                   epoch=epoch,
                                   loss_runner=loss_runner,
                                   wrong_runner=wrong_runner,
                                   error_runner=error_runner,
                                   batch_size=self.args.batch_size,
                                   pbar1=pbar1,
                                   cuda_device=cuda0,
                                   save=True)

        # Start testing
        print("\nStarting testing..")

        loss_runner = RunningAverage()
        wrong_runner = RunningAverage()
        error_runner = RunningAverage()

        model.test(model=densenet,
                   data=test_data,
                   loss_runner=loss_runner,
                   wrong_runner=wrong_runner,
                   error_runner=error_runner,
                   batch_size=self.args.batch_size,
                   cuda_device=cuda0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", "-d",
                         choices=["cifar10", "cifar100", "mnist", "svhn"],
                         help="""Choose one dataset to train and validate:
                                 cifar10, cifar100, mnist OR svhn.""")
    parser.add_argument("--epochs", type=int, default=30,
                         help="Number of traiing epochs (default=100)")
    parser.add_argument("--batch_size", type=int, default=128,
                         help="Batch size to use for training (default=32")
    parser.add_argument("--set_seed", type=int, default=12345,
                         help="Set the seed for reproducibility.")
    parser.add_argument("--optimizer", type=str, default="sgd",
                         help="Optimizer to use during training.")
    # parser.add_argument("--lr", type=float, default=1e-5,
    #                      help="Learning rate (default=1e-5)")
    args = parser.parse_args()

    dccnn = DenselyConnectedCNN(args=args)
    dccnn.main()
