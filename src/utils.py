class RunningAverage(object):
    def __init__(self):
        self.sum = 0.0
        self.count = 0.0
        self.average = 0.0

    def update(self, val, N):
        self.sum += float(val * N)
        self.count += N
        self.average = self.sum / float(self.count)
